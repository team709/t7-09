/** 
* 
* @author Venkat Raghava Sampath
*
*/

const mongoose = require('mongoose')
var Schema = mongoose.Schema;

const OrderSchema = new mongoose.Schema({

  _id: 
    {
     
  // orderID: {
    type: Number,
    required: true,
    unique: true,
    
  },
  
  datePlaced: {
    type: Date,
    required: true,
    default: Date.now()
  },
  dateShipped: {
    type: Date,
    required: true
  },
  paymentType: {
    type: String,
    required: true
  },
  paid: {
    type: Boolean,
    
  },
  customer_id: {
    required: true,
    type: Number

  }
})
module.exports = mongoose.model('Order', OrderSchema)
